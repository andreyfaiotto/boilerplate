# VTEX Proxy

## Pre-requisites

* Node - http://nodejs.org/

## Install

Clone this repo or download and unzip it.

## Quick Start

Change accountName and nickName in configs.json

Enter the folder you cloned or downloaded:

```shell
    cd `folder`
    npm install or npm i
```

First, open your browser here to authenticate:

https://{{accountName}}.vtexlocal.com.br:3000/admin/Site/Login.aspx

AND 

Access de page

https://{{accountName}}.vtexlocal.com.br:3000

## Run in developer environment

    npm run dev

## Run final files to production environment

    npm run prod
    

## Features

All files in `src/assets` are compiled, optimized and copied to `dist/` when you run `gulp`.


## Live Server Reload

    The `browserSync` is able to route files in the `/arquivos` and `/files` folders.