const fs            = require('fs');
const gulp          = require('gulp');
const clean         = require('gulp-clean');
const sass          = require('gulp-sass');
const minifyCSS     = require('gulp-cssmin');
const babel         = require('gulp-babel');
const uglify        = require('gulp-uglify-es').default;
const jshint        = require('gulp-jshint');
const imagemin      = require('gulp-imagemin');
const spritesmith   = require('gulp.spritesmith');
const sourcemaps    = require('gulp-sourcemaps');
const concat        = require('gulp-concat');
const autoprefixer  = require('gulp-autoprefixer');
const rename        = require('gulp-rename');
const browserSync   = require('browser-sync').create();
const runSequence   = require('run-sequence');
const vueify        = require('gulp-vueify2');
const config        = JSON.parse(fs.readFileSync('configs.json'));

let bases = {
    src:            './src',
    build:          './dist/arquivos',
    checkout:       './dist/files'

},

paths = {
    sass:           bases.src + '/assets/scss/*.scss',
    scripts:        bases.src + '/assets/js/**/*.js',
    css:            bases.src + '/assets/css/**/*.css',
    images:         bases.src + '/assets/img/**/*.+(png|jpg|jpge)',
    vendorsJS:      bases.src + '/assets/vendors/**/*.js',
    vendorsCSS:     bases.src + '/assets/vendors/**/*.css',
    checkoutCSS:    bases.src + '/assets/checkout/*.css',
    checkoutJS:     bases.src + '/assets/checkout/*.js',
    vuejs:          bases.src + '/assets/components/*.js',
    vue:            bases.src + '/assets/components/*.vue',
    svg:            bases.src + '/assets/svg/*.svg',
    copy:   [       
                bases.src + '/**/*.eot',
                bases.src + '/**/*.woff', 
                bases.src + '/**/*.woff2', 
                bases.src + '/**/fonts/**/*'
            ]
},
environment = undefined,
sassStyle = {},
setEnv = (env) => {
    environment = env
    
    if( env == 'prod' ) {
        sassStyle = {'sourcemap=none': true, noCache: true, outputStyle: 'compressed'}
    }
}

gulp.task('clean', () => {
    return gulp.src(bases.build)
        .pipe(clean())
})

gulp.task('copy', () => {
    gulp.src(paths.copy)
        .pipe(gulp.dest(bases.build))
})
gulp.task('copySVG', () => {
    gulp.src(paths.svg)
        .pipe(gulp.dest(bases.build))
})

gulp.task('components', () => {
    return gulp.src([paths.vuejs, 'App.vue.js'])
    .pipe(concat(config.accountName +'-components.min.js'))
    .pipe(uglify())
    .pipe(gulp.dest(bases.build));
});


gulp.task('vueify', () =>  {
    return gulp.src(paths.vue)
    .pipe(vueify())
    .pipe(gulp.dest(bases.build));
});

gulp.task('sass', () => {
    return gulp.src(paths.sass)
        .pipe(sourcemaps.init())
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(
			autoprefixer({
				overrideBrowserslist: ['last 10 versions' , 'safari 6' , 'ie >= 10' , 'ios 6' , 'android 4' , 'Firefox 14'],
				cascade: false
			})
        )
        .pipe(
			sourcemaps.write(".", {
				mapFile: function(mapFilePath) {
					return mapFilePath.replace(".css.map", ".map.css");
				}
			})
        )
        .pipe(gulp.dest(bases.build))
        .pipe(browserSync.stream())
})

gulp.task('lint', () => {
    return gulp.src(paths.scripts)
        .pipe(babel({presets: ['@babel/env']}))
        .pipe(jshint())
        .pipe(jshint.reporter('jshint-stylish'))
        .pipe(jshint.reporter('fail'))
});

gulp.task('scripts', ['browserReload'], () => {
    return gulp.src(paths.scripts)
        // .pipe(gulp.dest(bases.build))
        .pipe(babel({presets: ['@babel/env']}))
        .pipe(uglify())
        // .pipe(rename({ suffix: '.min' }))
        .pipe(gulp.dest(bases.build))
        .pipe(browserSync.stream())
})

gulp.task('images', () => {
    return gulp.src(paths.images)
        .pipe(imagemin({optimizationLevel: 7, progressive: true}))
        .pipe(gulp.dest(bases.build))
})


gulp.task('vendorsCSS', ['browserReload'], () => {
    return gulp.src(paths.vendorsCSS)
    .pipe(concat(config.accountName + '-vendor.min.css'))
    .pipe(minifyCSS())
    .pipe(gulp.dest(bases.build))
    .pipe(browserSync.stream())
});

gulp.task('vendorsJS', ['browserReload'], () => {
    return gulp.src(paths.vendorsJS)
    .pipe(concat(config.accountName + '-vendor.min.js'))
    .pipe(uglify())
    .pipe(gulp.dest(bases.build))
    .pipe(browserSync.stream())
});

gulp.task('maincss', () => {
    return gulp.src(paths.css)
    .pipe(autoprefixer({overrideBrowserslist: ['last 10 versions' , 'safari 6' , 'ie >= 10' , 'ios 6' , 'android 4' , 'Firefox 14'], cascade: false }))
    .pipe(minifyCSS())
    .pipe(gulp.dest(bases.build))
    .pipe(browserSync.stream())
});

gulp.task('checkoutcss', () => {
    return gulp.src(paths.checkoutCSS)
    .pipe(autoprefixer({overrideBrowserslist: ['last 10 versions' , 'safari 6' , 'ie >= 10' , 'ios 6' , 'android 4' , 'Firefox 14'], cascade: false }))
    .pipe(minifyCSS())
    .pipe(gulp.dest(bases.checkout))
    .pipe(browserSync.stream())
});

gulp.task('checkoutjs', () => {
    return gulp.src(paths.checkoutJS)
    .pipe(uglify())
    .pipe(gulp.dest(bases.checkout))
    .pipe(browserSync.stream())
});
gulp.task('vue', () => {
    return gulp.src(paths.vue)
    .pipe(uglify())
    .pipe(gulp.dest(bases.build))
    .pipe(browserSync.stream())
});



gulp.task('browserSync', () => {
    browserSync.init({
        open: false,
        https: config.https || true,
        host: config.accountName + '.vtexlocal.com.br',
        startPath: '/admin/login/',
        proxy: 'https://' + config.accountName + '.vtexcommercestable.com.br',
        serveStatic: [
            {
                route: '/arquivos',
                dir: [bases.build]
            },
            {
                route: '/files',
                dir: [bases.checkout]
            }
        ]
    })
})

gulp.task('watch', () => {
    gulp.watch( paths.sass,         ['sass'])
    gulp.watch( paths.css,          ['maincss'])
    gulp.watch( paths.scripts,      ['scripts'])
    gulp.watch( paths.vendorsJS,    ['vendorsJS'])
    gulp.watch( paths.vendorsCSS,   ['vendorsCSS'])
    gulp.watch( paths.checkoutJS,   ['checkoutjs'])
    gulp.watch( paths.checkoutCSS,  ['checkoutcss'])
    gulp.watch( paths.vuejs,        ['components'])
    gulp.watch( paths.images,       ['images'])
    gulp.watch( paths.fonts,        ['copy'])
    gulp.watch( paths.svg,          ['copySVG'])
})

gulp.task('browserReload', (cb) => {
    browserSync.reload()
    cb()
})

gulp.task('dev', () => {
    setEnv('dev')
    runSequence(
        'clean',
        'images',
        [
            'copy',
            'copySVG',
            'sass',
            'scripts',
            'vendorsJS',
            'vendorsCSS', 
            'checkoutjs',
            'checkoutcss',
            'maincss',
            'components',
            'vueify'
        ],
        'browserSync', 
        'watch'
    )
})

gulp.task('prod', () => {
    setEnv('prod')
    runSequence(
        'clean',
        'images',
        [
            'copy',
            'copySVG', 
            'sass', 
            'scripts',
            'vendorsJS', 
            'vendorsCSS', 
            'checkoutjs',
            'checkoutcss',
            'maincss',
            'components',
            'vueify'
        ]
    )
})

gulp.task('default', () => {
    console.log('Check package json to run tasks')
})