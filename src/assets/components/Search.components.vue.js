// register
let html = `
<div class="render-search">
   
    <div class="opacity-mask" @click="resetSearch()"></div>

    <form id="searchnapcom" class="header-search--form" action="?" method="get" @submit.stop.prevent="goSearchPage()">

        <div class="header-search--group">
        
            <input type="text" id="searchtyped" class="header-search--group-input" placeholder="Digite aqui a sua busca" autocomplete="off" v-model="search" v-on:keypress="beforeSearch()" />
            
            <input type="submit" class="header-search--group-button" id="searchsubmit" />

        </div>
        
        <div :rel="search.length" id="searchresult" class="search__tooltip" :class="{ active: search.length > 2 }" v-if="search.length > 2">

            <div class="content_box-search" :rel="products.length" v-if="products.length">

                <h5>Você está pesquisando:</h5>
                
                <ul class="list-results">
                    
                    <li class="item_context" v-for="item in categorys">
                        
                        <a :href="item.link" :title="item.name">
                            
                            <strong> {{ search }} </strong> em <strong> {{ item.name }} </strong>
                            
                        </a>
                        
                    </li>
                    
                </ul><!-- /.list-results -->
                
                <div class="features__head">
                        
                    <h5>Resultados recomendados:</h5>
                    
                </div><!-- /.features__head -->

                <div class="features-primary">
                    
                    <div class="feature-primary feature-primary--alt" v-for="item in products">
                        
                        <div class="feature__image">  
                            
                            <a :href="item.link" :title="item.name">
                                
                                <div class="product__image">
                                    
                                    <img :src="item.image" :alt="item.name"/>
                                    
                                </div><!-- /.product__image -->
                                
                            </a>
                            
                        </div><!-- /.feature__image -->
                        
                        <div class="feature__body">

                            <div class="feature__info">
                                
                                <a :href="item.link" :title="item.name"> 
                                    {{ item.name | limitLetter }} 
                                </a> 

                            </div>

                            <div class="feature__bottom">

                                <div class="feature__price"  v-if="item.bestPrice != 0">
                                    
                                    <h6>
                                        <span> 
                                            <small v-if="item.listPrice">{{ item.listPrice | formatMoney }}</small>
                                        </span>
                                        
                                        <em>
                                            <strong>{{ item.bestPrice | formatMoney }}</strong>
                                        </em>
                                    </h6>

                                </div>
                                
                                <div class="feature__btn-product" v-if="item.AvailableQuantity != 0">

                                    <a :href="item.link" :id="item.sku" class="cta-orange buy-buttom-assinc">Ver detalhes</a>

                                </div><!-- /.feature__btn-product -->

                                <div class="flag-unavaible" v-else>
                                
                                    <span>INDISPONÍVEL</span>

                                </div><!-- /.flag-unavaible -->

                            </div><!-- /.feature__bottom -->

                        </div><!-- /.feature__body -->

                    </div><!-- /.feature-primary -->

                </div><!-- /.features-primary -->
                
            </div>

            <div class="feature__not-found" v-else>
                
                <span>Por enquanto, não achamos nenhum produto.</span>

            </div>

        </div>
        
    </form>
    
</div>`;

Vue.component('search-component', {
    template: html,
    props: [
        'tapy',
        'id'
    ],
    data(){
        return {
            "search": "",
            "products": [],
            "categorys": [],
            "quantity": "1",
            "items": ""
        };
    },
	filters: {
		formatMoney(number){
            if (!number) return;

            number = (number + '').replace(',', '').replace(' ', '');
            
            let n = !isFinite(+number) ? 0 : +number,
                prec = !isFinite(+2) ? 0 : Math.abs(2),
                sep = (typeof "." === 'undefined') ? ',' : ".",
                dec = (typeof "," === 'undefined') ? '.' : ",",
                s = '',
                toFixedFix = function (n, prec) {
                    let k = Math.pow(10, prec);
                    return '' + Math.round(n * k) / k;
                };

            s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
            if (s[0].length > 3) {
                s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
            }

            if ((s[1] || '').length < prec) {
                s[1] = s[1] || '';
                s[1] += new Array(prec - s[1].length + 1).join('0');
            }

            return "R$" + s.join(dec);
        },
        limitLetter(string){
            if (!string) return;

            if (string.length > 50) {
            	return string.substring(0, 50) + " ...";
            } else {
            	return string.substring(0, 50);
            }
        }
	},
    methods: {
    	beforeSearch(){
    		clearInterval(window.interval);

            window.interval = setInterval(() => {
                clearInterval(window.interval);
				
				if (this.search.length > 2) {

                    this.getProducts();

                    document.querySelector('.render-search').classList.add('search-active');
                    document.querySelector('.features__head').classList.add('active');

	    		} else {

                    this.clearSearch();
                    
                    document.querySelector('.render-search').classList.remove('search-active');
                    document.querySelector('.features__head').classList.remove('active');
                    
	    		}

            }, 100);
    	},
    	getProducts(){
    		let _self = this;

    		$.get("/api/catalog_system/pub/products/search?ft=" + this.search + "&_from=0&_to=20", (products) => {
                _self.resultSearch(products);
			});
    	},
    	resultSearch(products){

            // console.log("products ->", products);

			if (products.length) {

                this.clearSearch();

	    		for (let i = 0; i < products.length; i++) {

                    // console.log("name ->", this.formatLinkCategory(products[i].categories))

			    	if (!this.inArray(this.formatNameCategory(products[i].categories))){
                       
			    		this.categorys.push({
			    			"name": this.formatNameCategory(products[i].categories),
			    			"link": this.formatLinkCategory(products[i].categories)
			    		})
                    }

			    	this.products.push({
			    		"sku": products[i].items[0].itemId,
			    		"name": products[i].productName,
			    		"brand": products[i].brand,
			    		"link": products[i].link,
			    		"image": products[i].items[0].images[0].imageUrl,
			    		"listPrice": this.formatDiscount(products[i].items[0].sellers[0].commertialOffer.ListPrice, products[i].items[0].sellers[0].commertialOffer.Price),
                        "bestPrice":  products[i].items[0].sellers[0].commertialOffer.Price,
                        "AvailableQuantity": products[i].items[0].sellers[0].commertialOffer.AvailableQuantity
                    });
                    
                }
                
            } else {

                

            }
            
    	},
        formatDiscount(listPrice, bestPrice){
            if (listPrice != bestPrice)
                return listPrice;
        },
    	goSearchPage() {

            if(this.search) {

                // console.log("search ->", this.search);

                location.href = location.origin + "/" + this.search;

                // location.href = location.origin + "/" 
            } else {

                alert("Por favor digite o que você procura na busca!");
                return;

            }
        },
        resetSearch(){
            document.querySelector('#searchtyped').value = '';
            document.querySelector("#searchresult").classList.remove("active");
            document.querySelector(".render-search").classList.remove("search-active");
            document.querySelector(".features__head").classList.remove("active");
            
            this.search = '';
            this.clearSearch();
        },
    	clearSearch(){
			this.products = [];
			this.categorys = [];
        },
        goToProduct(sku) {

            // console.log("selected", sku)
            
            let item = {
                id: sku,
                quantity: this.quantity,
                seller: "1"
            };

        },
    	inArray(category){
    		if (!category) return;

    		return (this.categorys.filter((item) => { 
    			return item.name == category 
    		})).length > 0  ? true : false;
    	},
    	formatNameCategory(category) {
    		return category[0].split("/")[1];
    	},
    	formatLinkCategory(category) {

            // console.log("category", category);

            return this.rewriteAll(category[0].split("/")[1]) + "?ft=" + this.search;
            
    	},
    	rewrite(str) {
            if (!str) return;
  
            return str.toLowerCase().trim()
                .replace(/[áàãâä]/g, "a")
                .replace(/[éèẽêë]/g, "e")
                .replace(/[íìĩîï]/g, "i")
                .replace(/[óòõôö]/g, "o")
                .replace(/[úùũûü]/g, "u")
                .replace(/ç/g, "c")
                .replace(/(\ |_)+/, " ")
                .replace(/(^-+|-+$)/, "")
                .replace(/[ ]/g, "-")
        },
        toggleClass(){
            // this.multiple.active = !this.multiple.active;
        },
        addItemSearch(){
            // this.multiple.items.push({
            //     "name": this.multiple.term.split(",")[0],
            //     "rewrite": this.rewrite(this.multiple.term)
            // });

            // this.multiple.term = "";
        },
        removeAllItens(){
            // this.multiple.items = [];
        },
        removeItrem(value){
            // this.multiple.items = this.multiple.items.filter((item) => {
            //     return item.name != value.name;
            // });
        },
        rewrite (str){
            // if (!str) return;

            // return str.toLowerCase()
            //     .replace(/[áàãâä]/g, "a")
            //     .replace(/[éèẽêë]/g, "e")
            //     .replace(/[íìĩîï]/g, "i")
            //     .replace(/[óòõôö]/g, "o")
            //     .replace(/[úùũûü]/g, "u")
            //     .replace(/ç/g, "c")
            //     .replace(/(\ |_)+/, " ")
            //     .replace(/(^-+|-+$)/, "")
            //     .replace(/[^a-z0-9]+/g, "");
        },
        rewriteAll (str) {

            if (!str) return;

            return str.toLowerCase()
                    .replace(/[áàãâä]/g, "a")
                    .replace(/[éèẽêë]/g, "e")
                    .replace(/[íìĩîï]/g, "i")
                    .replace(/[óòõôö]/g, "o")
                    .replace(/[úùũûü]/g, "u")
                    .replace(/ç/g, "c")
                    .replace(/(\ |_)+/, " ")
                    .replace(/(^-+|-+$)/, "")
                    .replace(/[/]/g, "")
                    .replace(/[^a-z0-9]+/g, '-');

        }
    },
});
